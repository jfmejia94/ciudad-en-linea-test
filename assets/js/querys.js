$( document ).ready(function() {
   
   $('#send-register').click(function(){
   		var validator = $("#formSend").validate({
   			rules: {
   				fisrtname:{
					required: true,
	          	},
   				lastname:{
				  required: true,
   				},
			    number: {
			      required: true,
			      number: true
			    },
			    email: {
			      required: true,
			      email: true
			    }
			},
			messages: {
				fisrtname:{
					required: "Campo requerido para continuar",
	          	},
				lastname:{
					required: "Campo requerido para continuar",
	          	},
	            number: {
	              required: "Campo requerido para continuar",
	              number: "Solo se Aceptan numeros"
	            },
	            email: {
	              required: "Campo requerido para continuar"
	            }
			}
		});

		validator.element("#number-document");
		validator.element("#fisrtname");
		validator.element("#lastname");
		validator.element("#email");

		

		if ($('#formSend').valid()){

			$('.send-button').hide();
			$('.loading-send').show();

			var data = {
				type_document: $('#tipodocument').val() ,
				document_number: $('#number-document').val() ,
				first_name: $('#fisrtname').val() ,
				last_name: $('#lastname').val() ,
				email: $('#email').val(),
			};


			$.ajax({
	            type: 'POST',
	            url: 'registrar-usuario',
	            data: data,
	            success: function(data) {
	               	console.log(data);
	               	setTimeout(function(){ 
	               		$('#tipodocument').val('0');
						$('#number-document').val('') ;
						$('#fisrtname').val('') ;
						$('#lastname').val('');
						$('#email').val('');

						$('.loading-send').hide();
	               		$('.send-button').show();
	               		$('.alert-success').show();
	               	}, 4000);
	               	
	               	setTimeout(function(){ 
	               		$('.alert-success').hide();
	               	}, 5000);
	                //$("#planes-db").html(data)
	            }
	        });
		}
		
   });	

	$('.inscribe-button').click(function(){

			var data = {
				user_id: $(this).data('id') ,
				id_course: $('#id-course').val() ,
			};

			var dataemail = $(this).data('email');

			$.ajax({
	            type: 'POST',
	            url: '../inscribir-user',
	            data: data,
	            success: function(data) {
	            	console.log(data);
	        		if (data === 'true') {
	        			//alert('se inscribio al curso correctamente');
	        			$('.alert-success').show();
	        			$('.email-inscrito').html(dataemail);

        				setTimeout(function(){ 
	               			$('.alert-success').hide();
	               		}, 5000);
	        		};

	        		if (data === 'false') {
	        			$('.alert-warning').show();
	        			$('.email-inscrito').html(dataemail);
	        			
        				setTimeout(function(){ 
	               			$('.alert-warning').hide();
	               		}, 5000);
	        		};
	        	
	            }
	        });
	});
});