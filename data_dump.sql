-- MySQL Workbench Synchronization
-- Generated: 2016-03-27 23:47
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Freddy

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `jhondeve_ciudadenlinea`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `type_document` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `document_number` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  `first_name` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  `last_name` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  `email` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  `register_date` TIMESTAMP NULL DEFAULT NULL COMMENT '',
  `update_date` TIMESTAMP NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `jhondeve_ciudadenlinea`.`courses_and_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT(11) NOT NULL COMMENT '',
  `id_course` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_courses_and_users_user_idx` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_courses_and_users_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `jhondeve_ciudadenlinea`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
