<?php

class Courses_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function save_user($params) {
        $data = array(
            'type_document' => $params['type_document'],
            'document_number' => $params['document_number'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email']
        );

        $this->db->select('email');
        $this->db->from('user');
        $this->db->where('email', $params['email']);
        $query = $this->db->get();

        if (count($query->result()) > 0) {
            $this->db->where('email', $params['email']);
            if (!$this->db->update('user', $data)) {
                return false;
            } else {
                $this->db->select('id');
                $this->db->from('user');
                $this->db->where('email', $params['email']);
                $id_user = $this->db->get()->result();
                return $id_user[0]->id;
            }
        } else {
            if (!$this->db->insert('user', $data)) {
                return false;
            }
            return $this->db->insert_id();
        }
    }
    function listUsers() {
        $this->db->select('*'); // SELECT * FROM
        $this->db->from('user'); // tabla nuevo_foro
        $query = $this->db->get(); // Obtiene resultados.
        return $query->result(); //Retorna al controllador un arreglo con los valores del query
    }

    function getUserIncribe($ids) {
        $this->db->select('*'); // SELECT * FROM
        $this->db->from('courses_and_users'); // tabla nuevo_foro
        $this->db->join('user', 'user.id = courses_and_users.user_id');
        $this->db->where('id_course', $ids);
        $query = $this->db->get(); // Obtiene resultados.
        return $query->result(); //Retorna al controllador un arreglo con los valores del query
    }


    function saveInscribe($params){
        $data = array(
            'user_id' => $params['user_id'],
            'id_course' => $params['id_course'],
        );

        $this->db->select();
        $this->db->from('courses_and_users');
        $this->db->where('user_id', $params['user_id']);
        $this->db->where('id_course', $params['id_course']);
        $query = $this->db->get();

        
        if (count($query->result()) > 0) {
            return false;
        } else {
            if (!$this->db->insert('courses_and_users', $data)) {
                return false;
            }
            return  true;
        }
    }
}

?>
