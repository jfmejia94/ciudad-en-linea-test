<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {


	public function index()
	{

		$api = 'https://api.coursera.org/api/';
		$url = $api . 'courses.v1?fields=description,photoUrl';
		// make the request
		$response = file_get_contents($url);

		$courses = json_decode($response, true);
		$data['info'] =  $courses['elements'];

		$this->load->helper('url');
		$this->load->view('home', $data);
	}

	public function viewcourse($id){
		//$this->load->model('courses_model');
		$this->load->helper('url');
		$api = 'https://api.coursera.org/api/';
		$url = $api . 'courses.v1/'.$id.'/?fields=partnerLogo,description,photoUrl';

		$response = file_get_contents($url);
		$InfoCourse = json_decode($response, true);
		

		$listusersInscribe = $this->courses_model->getUserIncribe($id);
		$list = $this->courses_model->listUsers();

		$data['info'] =  $InfoCourse['elements'];
		$data['listUsers'] = $list;
		$data['usersIncritos'] = $listusersInscribe;

		$this->load->view('viewCourse',$data);

	}

	public function saveUser(){
		$this->load->model('courses_model');
		$post = $_POST;
		$data = $this->courses_model->save_user($post);
		echo json_encode($data);
	}

	public function UserCourse(){
		$this->load->model('courses_model');
		$post = $_POST;

		$data = $this->courses_model->saveInscribe($post);
		echo json_encode($data);
	}
}
