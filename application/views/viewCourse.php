<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
	<link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.jpg"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.12.2.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/querys.js"); ?>"></script>

	<meta charset="utf-8">
	<title>Detalle del curso</title>
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	
</head>
       <script type="text/javascript">

	    $(document).ready(function() {
	    console.log('hola');
	        document.title = <?php echo $info[0]['name'];?>;
	    });
	
	</script>
<div class="container marketing">  
	<ol class="breadcrumb">
	  <li><a href="../">Listado de Cursos</a></li>
	  <li class="active"><?php echo $info[0]['name'];?></li>
	</ol>

	<?php //var_dump($info[0]);?>
	<div class="row featurette">
		<div class="col-md-7">
		<input type="hidden" id="id-course" value="<?php echo $info[0]['id'];?>">
		  <h2 class="featurette-heading"><?php echo $info[0]['name'];?> </h2>
		  <p class="lead"><?php echo $info[0]['description'];?></p>
		  <div class="row">
		  	<h3><strong>Candidatos a Inscribirse</strong></h3>
		  	<div class="alert alert-success" style="display:none;">
			  <strong class="email-inscrito"></strong> quedo correctamente inscrito al curso.
			</div>
			<div class="alert alert-warning" style="display:none;">
			  <strong class="email-inscrito"></strong> ya esta inscrito en este curso.
			</div>
		  	<table class="table">
			 	<tr>
				    <th>Nombres y Apellidos</th>
				    <th>Correo Electronico</th> 
			  	</tr>
				<tr>
					<?php foreach($listUsers as $infoo): ?>
						 	<tr>
							    <td><?php echo $infoo->first_name.'  '.$infoo->last_name ?></td>
							    <td><?php echo $infoo->email; ?></td>
							    <td><button class="inscribe-button btn btn-info" data-email="<?=$infoo->email?>" data-id="<?=$infoo->id;?>">Inscribir al curso</button></td>
							</tr>
					<?php endforeach;?>
				</tr>
			</table>
		  </div>
		  <div class="row">
		  	<h3><strong>Candidatos que estan en el Curso</strong></h3>
		  	<table class="table">
			 	<tr>
				    <th>Nombres y Apellidos</th>
				    <th>Correo Electronico</th> 
			  	</tr>
				<tr>
					<?php foreach($usersIncritos as $infoo): ?>
						 	<tr>
							    <td><?php echo $infoo->first_name.'  '.$infoo->last_name ?></td>
							    <td><?php echo $infoo->email; ?></td>
							</tr>
					<?php endforeach;?>
				</tr>
			</table>
		  </div>
		</div>
		<div class="col-md-5">
		  <img class="featurette-image img-responsive center-block" width="300" src="<?php echo $info[0]['partnerLogo'];?>"  data-holder-rendered="true">
		</div>
	</div>
</div>
</body>