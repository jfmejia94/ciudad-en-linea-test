<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>

	<link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.jpg"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.12.2.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/querys.js"); ?>"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.js"></script>

	<meta charset="utf-8">
	<title>Listado de Cursos</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div class="page-header">
	<h1>Listado de Cursos</h1>
	<p style="margin-left:5px">Si deseas Inscribirte en algun curso es necesario registrarse. haz clic en registrarse</p>      
  	<button type="button" id="register" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Registrarse</button>
</div>

<div id="container">
	<table class="table">
	 <tr>
	    <th>Logo Curso</th>
	    <th>Nombre Curso</th> 
	    <th>Descripcion</th> 
	  </tr>
	<tr>
		<?php  foreach($info as $infoo): ?>
		 	<tr>
			    <td><img width="100" src="<?=$infoo['photoUrl'];?>"/></td>
			    <td><a href="<?= base_url(); ?>ver-curso/<?=$infoo['id'];?>"><?=$infoo['name'];?></a></td>
			    <td><?= (strlen($infoo['description']) > 200) ? substr($infoo['description'], 0, 200) . '...' : $infoo['description'];?></td>
			</tr>
		<?php endforeach;?>
	</tr>
	</table>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">A continuaci贸n algunos datos</h4>
      </div>
      <div class="modal-body">
       <form role="form" id="formSend">
   		<div class="form-group">
			<label for="inputdefault">Tipo de documento</label>
	   		<select class="form-control" name="tipodocument" id="tipodocument" required>
	   			<option value="0">Seleccione una opcion</option>
			 	<option value="T.I">T.I</option>
			  	<option value="C.C">C.C</option>
			  	<option value="C.E">C.E</option>
			</select>
   		</div>
		  <div class="form-group">
		    <input class="form-control input-sm" placeholder="Numero de Documento" id="number-document" name="number" type="text" required>
		  </div>
		  <div class="form-group">
		    <input class="form-control input-sm" placeholder="Nombre" id="fisrtname" type="text" name="fisrtname" required>
		  </div>
		  <div class="form-group">
		    <input class="form-control input-sm" placeholder="Apellidos" id="lastname" type="text" name="lastname" required>
		  </div>
		  <div class="form-group">
		    <input class="form-control input-sm" placeholder="Email" id="email" name="email" type="email" required> 
		  </div>
		  <img width="100" class="loading-send" style="display:none; text-align:center;"src="<?php echo base_url(); ?>assets/images/loading.gif">
		  <button type="button" id="send-register" class="btn btn-second send-button" >Registrarse</button>
		  	<br>
		  	<br>
		  	<div class="alert alert-success" style="display:none;">
			  <strong>Correcto tu registro!</strong> Ahora ya puedes inscribirte en algun curso.
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>